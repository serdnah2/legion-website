<?php
function html5blank_customize_register($wp_customize){
  $wp_customize->add_section( 'html5-social', array(
      'title'          => __('Social Links', 'html5' ),
      'description'    => __('', 'html5' ),
      'priority'       => 35,
    ));

    $wp_customize->add_setting('html5_facebook',array(
      'default' => '',
      'sanitize_callback' => 'esc_url_raw',
    ));

    $wp_customize->add_control('html5_facebook',array(
      'label' => __('Facebook', 'html5' ),
      'section' => 'html5-social',
      'type' => 'text',
    ));
    
    $wp_customize->add_setting('html5_twitter',array(
        'default'           => '',
        'sanitize_callback' => 'esc_url_raw',
    ));

    $wp_customize->add_control('html5_twitter',array(
        'label' => __('Twitter', 'html5' ),
        'section' => 'html5-social',
        'type' => 'text',
    ));
    
    
    $wp_customize->add_setting('html5_footer_facebook_icon', array(
        'capability'        => 'edit_theme_options',
        'sanitize_callback' => 'esc_url_raw',
    ));
    
    $wp_customize->add_control( new WP_Customize_Image_Control($wp_customize, 'html5_footer_facebook_icon', array(
        'label'    => __('Footer facebook icon', 'html5'),
        'description'    => __('Upload/ Change Footer facebook icon', 'html5' ),
        'section'  => 'html5-social',
        'settings' => 'html5_footer_facebook_icon',
    )));
    
    $wp_customize->add_setting('html5_footer_twitter_icon', array(
        'capability'        => 'edit_theme_options',
        'sanitize_callback' => 'esc_url_raw',
    ));
    
    $wp_customize->add_control( new WP_Customize_Image_Control($wp_customize, 'html5_footer_twitter_icon', array(
        'label'    => __('Footer twitter icon', 'html5'),
        'description'    => __('Upload/ Change Footer twitter icon', 'html5' ),
        'section'  => 'html5-social',
        'settings' => 'html5_footer_twitter_icon',
    )));
    
    
     $wp_customize->add_section( 'html5_general_settings', array(
         'title'          => __('General Settings', 'html5' ),
        'description'    => __('Add/Edit General Settings of Site Here', 'html5' ),
        'priority'       => 35,
    ) );

    $wp_customize->add_setting('html5_site_logo', array(
        'capability'        => 'edit_theme_options',
        'sanitize_callback' => 'esc_url_raw',
    ));
    
    $wp_customize->add_control( new WP_Customize_Image_Control($wp_customize, 'html5_site_logo', array(
        'label'    => __('Site Logo', 'html5'),
        'description'    => __('Upload/ Change Site Logo Here', 'html5' ),
        'section'  => 'html5_general_settings',
        'settings' => 'html5_site_logo',
    )));
    
    $wp_customize->add_setting('html5_footer_text', array(
        'capability'     => 'edit_theme_options',
        'default' => __('', 'html5' ),
        'sanitize_callback' => 'sanitize_text_field'
    ));

    $wp_customize->add_control('html5_footer_text', array(
        'label'       => __('Footer text', 'html5'),
        'description' => __('Add/Edit Footer text Here', 'html5'),
        'section'     => 'html5_general_settings',
        'settings'    => 'html5_footer_text',
        'type' => 'textarea'
    ));
    
    $wp_customize->add_setting('html5_address', array(
        'capability'     => 'edit_theme_options',
        'default' => __('', 'html5' ),
        'sanitize_callback' => 'sanitize_text_field'
    ));

    $wp_customize->add_control('html5_address', array(
        'label'       => __('Address', 'html5'),
        'description' => __('Add/Edit Address Here', 'html5'),
        'section'     => 'html5_general_settings',
        'settings'    => 'html5_address',
        'type' => 'textarea'
    ));
    
    $wp_customize->add_setting('html5_phone', array(
        'capability'     => 'edit_theme_options',
        'default' => __('', 'html5' ),
        'sanitize_callback' => 'sanitize_text_field'
    ));

    $wp_customize->add_control('html5_phone', array(
        'label'       => __('Phone', 'html5'),
        'description' => __('Add/Edit Phone Here', 'html5'),
        'section'     => 'html5_general_settings',
        'settings'    => 'html5_phone',
    ));
    
    $wp_customize->add_setting('html5_email', array(
        'capability'     => 'edit_theme_options',
        'default' => __('', 'html5' ),
        'sanitize_callback' => 'sanitize_text_field'
    ));

    $wp_customize->add_control('html5_email', array(
        'label'       => __('Email', 'html5'),
        'description' => __('Add/Edit Email Here', 'html5'),
        'section'     => 'html5_general_settings',
        'settings'    => 'html5_email',
    ));
}

?>