<section class="team-section1" style="background-image: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) )?>')">
  <article class="montserratbold">
    <?php echo get_field('team_top_title');?>
  </article>
</section>
<section class="sub-menu-wrapper">
  <div class="montserratregular sub-menu">
    <?php html5blank_nav('sub menu'); ?>
  </div>
</section>

<section class="team-section2">
  <article>
    <h1 class="montserratregular">
      <?php echo get_field('team_title');?> <br/>
    </h1>
    <span class="montserratregular">
      <?php echo get_field('team_desc');?> <br/>
    </span>
  </article>
</section>

<section class="team-section3">
   <div>
      <?php 
         $category_id = get_cat_ID('team');
         $args = array(
             'offset' => 0,
             'category' => $category_id,
             'orderby' => 'post_date',
             'order' => 'ASC',
             'post_type' => 'post',
         );
         $team = wp_get_recent_posts($args, ARRAY_A);
        ?>
      <?php foreach ($team as $item) : ?>
      <div class="team-wrapper" style="background-image: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id( $item['ID'] ))?>')">
         <div class="team-container">
            <div class="montserratbold team-section">
               <span><?php echo get_field('team_picture_name', $item['ID']); ?></span>
               <br/><?php echo get_field('team_picture_position', $item['ID']); ?><br/>
            </div>
         </div>
      </div>
      <?php endforeach; ?>
   </div>
</section>

<section class="about-section4" style="background-image: url('<?php echo get_field('get_in_touch_background')['url']?>')">
  <article>
    <div>
      <span class="montserratregular">
        <?php echo get_field('team_get_desc');?>
      </span>
      <h1 class="montserratsemi_bold get-in-touch-btn">
          <button type="button" class="montserratregular big-button button-orange" onclick="location.href='<?php echo get_field('get_in_touch_link'); ?>';">
            <?php echo get_field('team_get_title');?> <br/>
          </button>
      </h1>
    </div>
  </article>
</section>