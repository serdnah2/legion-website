(function ($, root, undefined) {
	$(function () {
		'use strict';

		var createdEvent = false;
		// DOM ready, take it away
		$('.home-video-play-img img').click(function () {
			$('#home-video-play').hide();
			$('#home-video').css('opacity', '1');
			var video = document.getElementById("home-video");
			video.play();
			if (!createdEvent) {
				createdEvent = true;
				video.addEventListener('ended', function (e) {
					if (!e) { e = window.event; }
					$('#home-video').css('opacity', '0');
					$('#home-video-play').show();
				}, false);
			}
		});

		$('.flexslider').flexslider({
			animation: "slide"
		});

		$('.work-details-section1-arrow-down img').click(function () {
			$('html,body').animate({
				scrollTop: $($(".sub-menu-wrapper")[0]).offset().top
			}, 'slow');
		});

		$('#menu-mobile').click(function(){
			$(this).toggleClass('is-active');
		});

	});

})(jQuery, this);
