<section class="services-section1" style="background-image: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) )?>')">
  <article class="montserratbold">
    <?php echo get_field('services_top_title');?>
  </article>
</section>

<section class="services-section2">
  <article>
    <h1 class="montserratregular"><?php echo get_field('services_ideas_title');?></h1>
    <span class="montserratsemi_bold">
      <?php echo get_field('services_ideas_desc');?>
    </span>
    <hr/>
    <div class="montserratsemi_bold">
      <div>
        <div class="svg-item-services" id="svg-0"></div>
        <div><?php echo get_field('services_planning_research_text');?></div>
      </div>
      <div>
        <div class="svg-item-services" id="svg-1"></div>
        <div><?php echo get_field('services_defining_design_text');?></div>
      </div>
      <div>
        <div class="svg-item-services" id="svg-2"></div>
        <div><?php echo get_field('services_bringing_text');?></div>
      </div>
    </div>
  </article>
</section>

<section class="services-section3">
  <article>
    <?php 
         $category_id = get_cat_ID('services_build');
         $args = array(
             'offset' => 0,
             'category' => $category_id,
             'orderby' => 'post_date',
             'order' => 'ASC',
             'post_type' => 'post',
         );
         $services = wp_get_recent_posts($args, ARRAY_A);
        ?>
      <?php foreach ($services as $service) : ?>
        <div class="services-build-item">
          <div class="services-build-item-txt">
            <div>
              <img src="<?php echo get_field('serices_build_items_img', $service['ID'])['url'];?>" alt="planing_&_research" height="100"/>
            </div>
            <div class="montserratregular">
              <section class="service-image-mobile" style="background-image: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id( $service['ID'] ))?>')"></section>
              <section class="service-txt">
                <section class="service-txt-wrapper">
                  <img class="icon-mobile" src="<?php echo get_field('serices_build_items_img', $service['ID'])['url'];?>" alt="planing_&_research" />
                  <section>
                    <h1 class="abril-fatfaceregular"><?php echo $service['post_title']; ?></h1>
                    <h2 class="montserratsemi_bold"><?php echo get_field('services_build_items_subtitle', $service['ID']);?></h2>
                  </section>
                </section>
                <?php echo $service['post_content']; ?>
              </section>
            </div>
          </div>
          <div class="service-image" style="background-image: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id( $service['ID'] ))?>')">
          </div>
      </div>
      <?php endforeach; ?>
      
  </article>
</section>

<section class="services-section4">
  <article>
    
    <div>
      <?php 
         $blog_id = get_cat_ID('blog_items');
         $argsBlog = array(
             'numberposts' => 4,
             'offset' => 0,
             'category' => $blog_id,
             'orderby' => 'post_date',
             'order' => 'ASC',
             'post_type' => 'post',
         );
         $blog_items = wp_get_recent_posts($argsBlog, ARRAY_A);
        ?>
        <?php foreach ($blog_items as $blog_item) : ?>
 	        <div style="cursor: pointer; background-image: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id( $blog_item['ID'] ))?>')" onclick="location.href='<?php echo get_permalink($blog_item['ID']);?>';">
            <div class="service-item montserratregular">
              <h5><?php echo $blog_item['post_title']; ?></h5>
              <h5><?php echo mysql2date('j M Y', $blog_item['post_date']); ?></h5>
            </div>
          </div>
        <?php endforeach; ?>
    </div>
  </article>
</section>

<script>
  new Vivus('svg-0', {duration: 200, file: '<?php echo get_template_directory_uri(); ?>/images/services_0.svg'}, function(){
      console.log('end');
  });
  new Vivus('svg-1', {duration: 200, file: '<?php echo get_template_directory_uri(); ?>/images/services_1.svg'}, function(){
      console.log('end');
  });
  new Vivus('svg-2', {duration: 200, file: '<?php echo get_template_directory_uri(); ?>/images/services_2.svg'}, function(){
      console.log('end');
  });
</script>