<section class="home-section-slider">
   <div class="flexslider">
      <ul class="slides">
         <?php
            $home_top_slider_id = get_cat_ID('home_top_slider');
            $args_home_top_slider = array(
                'offset' => 0,
                'category' => $home_top_slider_id,
                'orderby' => 'post_date',
                'order' => 'ASC',
                'post_type' => 'post',
            );
            $sliders_top = wp_get_recent_posts($args_home_top_slider, ARRAY_A);
            ?>
         <?php foreach ($sliders_top as $slider) : ?>
         <li>
            <div class="section-slider-item-wrapper" style="background-image: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id( $slider['ID'] ))?>')">
                <article>
                    <span class="abril-fatfaceregular"><?php echo get_field('slide_title', $slider['ID']); ?></span> <br/>
                    <span class="montserratbold"><?php echo get_field('slide_subtitle', $slider['ID']); ?></span> <br/>
                    <div class="proxima-nova-ltsemibold">
                        <?php echo get_field('slide_description', $slider['ID']); ?>
                    </div>
                    <div>
                        <?php $learn_more = get_field('learn_more_button_text', $slider['ID']); ?>
                        <?php $contact = get_field('contact_us_button_text', $slider['ID']); ?>
                        <?php $link = get_field('lear_more_button_link', $slider['ID']); ?>
                        <?php
                            $url = '';
                            if($link && $link !== ''){
                                $url = $link;
                            }else{
                                $url = get_permalink($slider['ID']);
                            }
                        ?>
                        <?php if($learn_more && $learn_more !== '') :?>
                            <button type="button" class="proxima-nova-ltsemibold big-button button-white" onclick="location.href='<?php echo $url;?>';"><?php echo $learn_more; ?></button>
                        <?php endif;?>
                        <?php if($contact && $contact !== '') :?>
                            <button type="button" class="proxima-nova-ltsemibold big-button button-orange" onclick="window.location.href='contact-us'"><?php echo $contact; ?></button>
                        <?php endif;?>
                    </div>
                </article>
            </div>
         </li>
         <?php endforeach; ?>
      </ul>
      <img class="winner-img" src="<?php echo get_template_directory_uri(); ?>/images/winner.png"/>
   </div>
</section>
<section class="home-section2">
   <div class="montserratlight home-section2-top-clients-title"><?php echo get_post_meta( get_the_ID(), 'home_selected_clients', true) ?></div>
   <div class="home-section2-top-clients">
      <?php
         $clients_id = get_cat_ID('top_clients');
         $argsClients = array(
             'offset' => 0,
             'category' => $clients_id,
             'orderby' => 'post_date',
             'order' => 'ASC',
             'post_type' => 'post',
         );
         $clients = wp_get_recent_posts($argsClients, ARRAY_A);
         ?>
      <?php foreach ($clients as $client) : ?>
      <div class="home-section2-top-clients-item" style="background-image: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id( $client['ID'] ))?>')"></div>
      <?php endforeach; ?>
   </div>
</section>
<section class="home-section3">
   <div class="home-section3-bottom-clients">
      <?php
         $clients_bottom_id = get_cat_ID('bottom_clients');
         $argsClientsBottom = array(
             'offset' => 0,
             'category' => $clients_bottom_id,
             'orderby' => 'post_date',
             'order' => 'ASC',
             'post_type' => 'post',
         );
         $clientsBottom = wp_get_recent_posts($argsClientsBottom, ARRAY_A);
         ?>
      <?php foreach ($clientsBottom as $clientBottom) : ?>
      <div class="home-section3-bottom-clients-item" style="background-image: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id( $clientBottom['ID'] ))?>')"></div>
      <?php endforeach; ?>
   </div>
</section>
<section class="home-section4" style="background-image: url('<?php echo get_field('home_handcraft')['url']?>')">
   <div class="home-section4-wrapper">
      <div class="home-section4-header">
         <div class="abril-fatfaceregular home-section4-header-title">
             <h1><?php echo get_field('home_handcraft_title');?></h1>
         </div>
         <span class="montserratregular">
            <h2><?php echo get_field('home_handcraft_desc');?></h2>
         </span>
      </div>
      <div class="home-section4-content">
          <?php
            $hancraft_id = get_cat_ID('hancraft');
            $args_hancraft = array(
                'offset' => 0,
                'category' => $hancraft_id,
                'orderby' => 'post_date',
                'order' => 'ASC',
                'post_type' => 'post',
            );
            $hancrafts = wp_get_recent_posts($args_hancraft, ARRAY_A);
            ?>
            <?php $index = 0;?>
            <?php foreach ($hancrafts as $hancraft) : ?>
                <div class="home-section4-content-item">
                    <div class="home-section4-content-item-img">
                        <div class="svg-item" id="svg-<?php echo $index; ?>"></div>
                        <!--<img height="100px" src="<?php echo wp_get_attachment_url( get_post_thumbnail_id( $hancraft['ID'] ))?>" />-->
                    </div>
                    <div class="home-section4-content-item-txt">
                        <div class="home-section4-content-item-txt-title">
                           <?php echo $hancraft['post_title'];?>
                        </div>
                        <p class="montserratregular">
                            <?php echo $hancraft['post_content'];?>
                        </p>
                    </div>
                </div>
                <?php $index++; ?>
            <?php endforeach; ?>
      </div>
      <div class="home-section4-button">
         <button type="button" class="montserratregular big-button button-orange"  onclick="window.location.href='services'"><?php echo get_field('home_handcraft_btn'); ?></button>
      </div>
   </div>
</section>
<section class="home-section5">
   <img class="placeholder-video" src="<?php echo get_template_directory_uri(); ?>/images/placeholder-video.jpg" />
   <video id="home-video">
      <source src="<?php echo get_field('home_video_src')['url']; ?>" type="video/mp4">
   </video>
   <div id="home-video-play" style="background-image: url('<?php echo get_field('home_video')['url']?>')">
      <div class="display-table">
         <div class="display-table-cell">
            <div class="home-video-play-img">
                <?php if(get_field('home_video_play')['url']):?>
                    <img src="<?php echo get_field('home_video_play')['url']; ?>" alt="play"/>
                <?php else:?>
                    <img src="<?php echo get_template_directory_uri(); ?>/images/play.svg" alt="play">
                <?php endif;?>
            </div>
            <div class="proxima-nova-ltsemibold home-video-play-txt">Play</div>
         </div>
      </div>
   </div>
</section>
<section class="home-section6">
   <div>
      <?php
         $category_id = get_cat_ID('home_cases');
         $args = array(
             'numberposts' => 8,
             'offset' => 0,
             'meta_key' => 'home_case_order',
             'category' => $category_id,
             'orderby' => 'meta_value',
             'order' => 'ASC',
             'post_type' => 'post',
         );
         $cases = wp_get_recent_posts($args);
        ?>
      <?php foreach ($cases as $case) : ?>
        <?php if(has_category('work_item', $case['ID'])):?>
            <div class="our-work" style="background-image: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id( $case['ID'] ))?>')">
                <div class="our-work-container">
                    <div class="montserratregular our-work-section">
                        <span class="montserratbold"><?php echo $case['post_title']; ?></span>
                        <br/><?php echo get_field('work_item_desc', $case['ID']); ?><br/>
                        <button type="button" class="button-case-study" onclick="location.href='<?php echo get_permalink($case['ID']);?>';">VIEW CASE STUDY</button>
                    </div>
                </div>
            </div>
        <?php endif;?>
        <?php if(has_category('blog_items', $case['ID'])):?>
            <div onclick="location.href='<?php echo get_permalink($case['ID']);?>';" class="our-work" style="cursor: pointer; background-image: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id( $case['ID'] ))?>')">
                <div class="our-work-container">
                    <div class="montserratregular our-work-section">
                        <span class="montserratbold"><?php echo $case['post_title']; ?></span>
                        <br/><?php echo mysql2date('j M Y', $case['post_date']); ?><br/>
                    </div>
                </div>
            </div>
        <?php endif;?>
      <?php endforeach; ?>
   </div>
</section>
<section class="home-section7" style="background-image: url('<?php echo get_field('testimonial_background')['url']?>')">
   <div class="flexslider">
      <ul class="slides">
         <?php
            $home_slider_id = get_cat_ID('home_txt_slider');
            $args_home_slider = array(
                'offset' => 0,
                'category' => $home_slider_id,
                'orderby' => 'post_date',
                'order' => 'ASC',
                'post_type' => 'post',
            );
            $sliders = wp_get_recent_posts($args_home_slider, ARRAY_A);
            ?>
         <?php foreach ($sliders as $slider) : ?>
         <li>
            <p class="montserratbold">
               <?php echo get_field('home_txt_slide', $slider['ID'])?>
            </p>
            <div class="montserratregular home-txt-slider-author">
               <?php echo get_field('home_txt_author', $slider['ID'])?>
            </div>
         </li>
         <?php endforeach; ?>
      </ul>
   </div>
</section>
<section class="home-section8" style="background-image: url('<?php echo get_field('home_work_us')['url']?>')">
   <div class="abril-fatfaceregular home_work_us_title"><?php echo get_field('home_work_us_title');?></div>
   <div class="montserratsemi_bold home_work_us_desc">
      <?php echo get_field('howe_work_us_desc');?>
   </div>
   <button type="button" class="montserratsemi_bold big-button button-orange"  onclick="window.location.href='contact-us'">
       <?php echo get_field('home_work_us_btn');?>
   </button>
</section>
<script>
  new Vivus('svg-0', {duration: 200, file: '<?php echo get_template_directory_uri(); ?>/images/hancraft_0.svg'}, function(){
      console.log('end');
  });
  new Vivus('svg-1', {duration: 200, file: '<?php echo get_template_directory_uri(); ?>/images/hancraft_1.svg'}, function(){
      console.log('end');
  });
  new Vivus('svg-2', {duration: 200, file: '<?php echo get_template_directory_uri(); ?>/images/hancraft_2.svg'}, function(){
      console.log('end');
  });
</script>