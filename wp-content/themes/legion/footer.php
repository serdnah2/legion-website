			<footer class="montserratregular">
				<div class="footer-container">
					<div class="footer-item-left">
						<div class="footer-logo-wrapper">
							<?php $sitelogo = get_theme_mod('html5_site_logo'); ?>
								<?php
								if (isset($sitelogo) && $sitelogo != '') {
									echo '<img class="footer-logo" src="'.$sitelogo.'" alt="logo" />';
								}
							?>
							<div>
								<?php echo get_theme_mod('html5_address'); ?>
							</div>
							<div>
								<?php echo get_theme_mod('html5_email'); ?>
							</div>
							<div>
								<?php echo get_theme_mod('html5_phone'); ?>
							</div>
							<div class="footer-section-wrapper-item">
								<?php $facebook_icon = get_theme_mod('html5_footer_facebook_icon'); ?>
								<?php $facebook_link = get_theme_mod('html5_facebook'); ?>
								<?php if (isset($facebook_link) && isset($facebook_icon) && $facebook_link != '' && $facebook_icon != ''):?>
									<a target="_blank" href="<?php echo $facebook_link;?>">
										<img src="<?php echo $facebook_icon;?>" />
									</a>
								<?php endif;?>

								<?php $twitter_icon = get_theme_mod('html5_footer_twitter_icon'); ?>
								<?php $twitter_link = get_theme_mod('html5_twitter'); ?>
								<?php if (isset($twitter_link) && isset($twitter_icon) && $twitter_link != ''  && $twitter_icon != ''):?>
									<a target="_blank" href="<?php echo $twitter_link;?>">
										<img src="<?php echo $twitter_icon;?>" />
									</a>
								<?php endif;?>
							</div>
						</div>
					</div>
					<div class="footer-item-right">
						<div class="footer-section-wrapper">
							<h4 class="montserratsemi_bold">Mobile Strategy</h4>
							<?php
								$mobiles_trategy_id = get_cat_ID('strategy');
								$mobiles_trategy_args = array(
									'category' => $mobiles_trategy_id,
									'orderby' => 'post_date',
									'order' => 'ASC',
									'post_type' => 'post',
								);
								$mobiles_trategys= wp_get_recent_posts($mobiles_trategy_args, ARRAY_A);
								?>
							<?php foreach ($mobiles_trategys as $mobiles_trategy) : ?>
							<div class="footer-section-wrapper-item">
								<?php echo $mobiles_trategy['post_title'];?>
							</div>
							<?php endforeach; ?>
						</div>
						<div class="footer-section-wrapper">
							<h4 class="montserratsemi_bold">Design</h4>
							<?php
								$design_trategy_id = get_cat_ID('design');
								$design_trategy_args = array(
									'category' => $design_trategy_id,
									'orderby' => 'post_date',
									'order' => 'ASC',
									'post_type' => 'post',
								);
								$design_trategys= wp_get_recent_posts($design_trategy_args, ARRAY_A);
								?>
							<?php foreach ($design_trategys as $design_trategy) : ?>
							<div class="footer-section-wrapper-item">
								<?php echo $design_trategy['post_title'];?>
							</div>
							<?php endforeach; ?>
						</div>
						<div class="footer-section-wrapper">
							<h4 class="montserratsemi_bold">Technology</h4>
							<?php
								$technology_trategy_id = get_cat_ID('technology');
								$technology_trategy_args = array(
									'category' => $technology_trategy_id,
									'orderby' => 'post_date',
									'order' => 'ASC',
									'post_type' => 'post',
								);
								$technology_trategys= wp_get_recent_posts($technology_trategy_args, ARRAY_A);
								?>
							<?php foreach ($technology_trategys as $technology_trategy) : ?>
							<div class="footer-section-wrapper-item">
								<?php echo $technology_trategy['post_title'];?>
							</div>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			</footer>
		</div>
		<?php wp_footer(); ?>
		<script>
		(function(f,i,r,e,s,h,l){i['GoogleAnalyticsObject']=s;f[s]=f[s]||function(){
		(f[s].q=f[s].q||[]).push(arguments)},f[s].l=1*new Date();h=i.createElement(r),
		l=i.getElementsByTagName(r)[0];h.async=1;h.src=e;l.parentNode.insertBefore(h,l)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-XXXXXXXX-XX', 'yourdomain.com');
		ga('send', 'pageview');
		</script>
	</body>
</html>
