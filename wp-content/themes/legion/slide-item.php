<section class="slide-item-section1" style="background-image: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) )?>')">
  <article class="montserratbold">
    <?php echo get_field('slide_title', get_the_ID() ); ?><br/>
  </article>
</section>

<section class="slide-item-section2" >
  <article>
    <?php echo the_content(); ?>
  </article>
</section>
