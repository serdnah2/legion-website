<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
		<link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
		<link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">

		<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/flexslider.css" media="screen" charset="utf-8">
		<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/legion.css" media="screen" charset="utf-8">

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		

		<?php wp_head(); ?>

		<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.flexslider.js"></script>
		<script src="<?php echo get_template_directory_uri(); ?>/js/lib/vivus.js"></script>
		<script src="<?php echo get_template_directory_uri(); ?>/js/lib/headroom.min.js"></script>
		<script>
		// conditionizr.com
		// configure environment tests
		conditionizr.config({
			assets: '<?php echo get_template_directory_uri(); ?>',
			tests: {}
		});

		document.addEventListener('DOMContentLoaded', function(event) {
			var subMenu = document.querySelector('.sub-menu-wrapper');
			var workDetails = document.querySelectorAll('.work-details-item');
			var top = subMenu !== null? subMenu.offsetTop: 0;
			window.subMenuFixed = false;
			window.pinned = 0;

			// grab an element
			var myElement = document.querySelector("header");
			// construct an instance of Headroom, passing the element
			var headroom  = new Headroom(myElement, {
				"offset": 100,
  				"tolerance": 10,
				onPin: function(){
					if(subMenu){
						subMenu.classList.remove('menu-top');	
					}
					
					window.pinned = -10;
				},
				onUnpin: function(){
					if(subMenu){
						subMenu.classList.add('menu-top');
					}
					if(window.innerWidth > 768){ 
						window.pinned = 70;
					}else{
						window.pinned = 54;
					}
				}
			});
			// initialise
			headroom.init(); 

			window.onscroll = function() {
				if (document.querySelector('body').scrollTop > 60 || document.querySelector('html').scrollTop > 60) {
					document.querySelector('header').classList.add('header-float');
				} else {
					document.querySelector('header').classList.remove('header-float');
				}

				if (subMenu) {
        			toggleWrapper(subMenu, top);
				}

				if (workDetails.length > 0) {
					selectMenuItem(workDetails);
				}
			};

			function toggleWrapper(element, top) {
				var offsetSubMenu = 0;
				if(window.innerWidth > 768){ 
					offsetSubMenu = window.pinned;
				}

				console.log(offsetSubMenu);

				if ( document.querySelector('body').scrollTop > (top - element.clientHeight + offsetSubMenu)  || document.querySelector('html').scrollTop > (top - element.clientHeight + offsetSubMenu)) {
					element.classList.add('sub-menu-wrapper-float');
					window.subMenuFixed = true;
				} else {
					element.classList.remove('sub-menu-wrapper-float');
					window.subMenuFixed = false;
				}
			}

			function selectMenuItem(workDetails) {
				var offsetSubMenu = 0;
				if(window.innerWidth > 768){ 
					offsetSubMenu = window.pinned;
				}
				for (var i in workDetails) {
					if (document.querySelector('body').scrollTop > workDetails[i].offsetTop - 150 + offsetSubMenu ||
							document.querySelector('html').scrollTop > workDetails[i].offsetTop - 150 + offsetSubMenu) {
						document.querySelectorAll('.sub-menu-item')[0].classList.remove('selected');
						document.querySelectorAll('.sub-menu-item')[1].classList.remove('selected');
						document.querySelectorAll('.sub-menu-item')[2].classList.remove('selected');

						document.querySelectorAll('.sub-menu-item')[i].classList.add('selected');
				  }
				}

				if (!window.subMenuFixed && 
					(document.querySelector('body').scrollTop < workDetails[0].offsetTop - 70 + offsetSubMenu|| 
					document.querySelector('html').scrollTop < workDetails[0].offsetTop - 70 + offsetSubMenu)) {
						document.querySelectorAll('.sub-menu-item')[0].classList.remove('selected');
				}
			}

			window.addEventListener("resize", function(){
				if(subMenu){
					subMenu.classList.remove('sub-menu-wrapper-float');
					window.subMenuFixed = false;
				}
				setTimeout(function(){
					subMenu = document.querySelector('.sub-menu-wrapper');
					workDetails = document.querySelectorAll('.work-details-item');
					top = subMenu !== null? subMenu.offsetTop: 0;
					if (subMenu) {
						toggleWrapper(subMenu, top);
					}
				}, 100);
			});
		});
		</script>

	</head>
	<body <?php body_class(); ?>>
		<div class="wrapper-page">
			<header>
				<?php $sitelogo = get_theme_mod('html5_site_logo'); ?>
					<?php if (isset($sitelogo) && $sitelogo != ''): ?>
						<div class="logo">
							<a href="<?php echo get_home_url(); ?>">
								<img src="<?php echo $sitelogo ?>"/>
							</a>
						</div>
					<?php endif;?>
				<button id="menu-mobile" class="c-hamburger c-hamburger--htx">
					<span>toggle menu</span>
				</button>
				<div class="montserratsemi_bold main-menu">
					<?php html5blank_nav('Top menu'); ?>
				</div>
			</header>
