<section class="blog-section1" style="background-image: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) )?>')">
  <article class="montserratbold">
    <?php echo get_post_meta( get_the_ID(), 'blog_section1_title', true) ?> <br/>
  </article>
</section>

<section class="blog-section2" >
  <article>
    <div>
      <?php
         $category_id = get_cat_ID('blog_items');
         $args = array(
             'offset' => 0,
             'category' => $category_id,
             'orderby' => 'post_date',
             'order' => 'ASC',
             'post_type' => 'post',
         );
         $blog_items = wp_get_recent_posts($args, ARRAY_A);
        ?>
      <?php foreach ($blog_items as $item) : ?>
      <div style="cursor: pointer; background-image: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id( $item['ID'] ))?>')" onclick="location.href='<?php echo get_permalink($item['ID']);?>';">
        <div class="montserratregular blog-item">
          <h5><?php echo $item['post_title']; ?></h5>
          <h5><?php echo mysql2date('j M Y', $item['post_date']); ?></h5>
        </div>
      </div>
      <?php endforeach; ?>
    </div>
  </article>
</section>
