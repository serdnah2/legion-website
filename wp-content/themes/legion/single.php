<?php get_header(); ?>
<main role="main">
	<?php if ( in_category('work_item')):?>
		<?php get_template_part('work-item'); ?>
	<?php endif;?>
	<?php if ( in_category('home_top_slider')):?>
		<?php get_template_part('slide-item'); ?>
	<?php endif;?>
	<?php if ( in_category('blog_items')):?>
		<?php get_template_part('blog-item'); ?>
	<?php endif;?>
</main>
<?php get_footer(); ?>
