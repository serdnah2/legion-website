<section>
  <?php if (have_posts()): while (have_posts()) : the_post(); ?>
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
      <div class="work-details-wrapper">
        <div class="work-details-section1" style="background-image: url('<?php echo get_field('work_details_top_img')['url']?>')">
          <div class="work-details-section1-overlay"></div>
          <?php
            $prev_post = get_previous_post(true);
            if (!empty( $prev_post )): ?>
              <div class="montserratlight work-details-section1-previous">
                <?php echo previous_post_link('%link', '<div class="work-details-section1-nav-label">PREVIOUS</div><div class="work-details-section1-nav-text">%title</div>', true, 'home_cases'); ?>
              </div>
          <?php endif ?>
          <?php
            $next_post = get_next_post(true);
            if (!empty( $next_post )): ?>
              <div class="montserratlight work-details-section1-next">
                <?php echo next_post_link('%link', '<div class="work-details-section1-nav-label">NEXT</div><div class="work-details-section1-nav-text">%title</div>', true, 'home_cases'); ?>
              </div>
          <?php endif ?>
          <div class="work-details-section1-top-info">
            <div class="montserratbold work-details-section1-top-info-title">
              <?php the_title(); ?>
            </div>
            <div class="montserratregular work-details-section1-top-info-sub-title">
              <?php echo get_field('work_item_desc');?>
            </div>
          </div>
          <div class="work-details-section1-arrow-down">
            <img src="<?php echo get_template_directory_uri(); ?>/images/arrow_down.png" alt=""/>
          </div>
        </div>
        <section class="sub-menu-wrapper">
          <div class="montserratregular sub-menu">
            <ul>
              <li class="sub-menu-item">
                <a onclick="goToWorkDetails(0)">1. Challenge</a>
              </li>
              <li class="sub-menu-item">
                <a onclick="goToWorkDetails(1)">2. Solution</a>
              </li>
              <li class="sub-menu-item">
                <a onclick="goToWorkDetails(2)">3. Result</a>
              </li>
            </ul>
          </div>
        </section>
        <div class="work-details-item">
          <div class="montserratregular work-details-item-number">
            <div>1</div>
          </div>
          <div class="abril-fatfaceregular work-details-item-title">
            <?php echo get_field('work_details_challenge_title');?>
          </div>
          <div class="montserratregular work-details-item-desc">
            <?php echo get_field('work_details_challenge_desc');?>
          </div>
        </div>
        <div class="work-details-item-img" style="background-image: url('<?php echo get_field('work_details_challenge_img')['url']?>')">
          <!--<img src="<?php echo get_field('work_details_challenge_img')['url'];?>" />-->
        </div>
        <div class="work-details-item">
          <div class="montserratregular work-details-item-number">
            <div>2</div>
          </div>
          <div class="abril-fatfaceregular work-details-item-title">
            <?php echo get_field('work_details_solution_title');?>
          </div>
          <div class="montserratregular work-details-item-desc">
            <?php echo get_field('work_details_solution_desc');?>
          </div>
        </div>
        <div class="montserratregular work-details-item-img-comment" style="background-image: url('<?php echo get_field('work_details_solution_img')['url']?>')">
          <div class="work-details-item-img-comment-data">
            <div class="work-details-item-img-comment-txt">
              <?php echo get_field('work_details_solution_comment');?>
            </div>
            <div class="work-details-item-img-comment-author">
              <?php echo get_field('work_details_solution_comment_author');?>
            </div>
          </div>
        </div>

        <div class="work-details-item">
          <div class="montserratregular work-details-item-number">
            <div>3</div>
          </div>
          <div class="abril-fatfaceregular work-details-item-title">
            <?php echo get_field('work_details_result_title');?>
          </div>
          <div class="montserratregular work-details-item-desc">
            <?php echo get_field('work_details_result_desc');?>
          </div>
        </div>
        <div class="work-details-item-img" style="background-image: url('<?php echo get_field('work_details_result_img')['url']?>')">
          <!--<img src="<?php echo get_field('work_details_result_img')['url'];?>" />-->
        </div>
      </div>
    </article>
  <?php endwhile; ?>
  <?php else: ?>
    <article>
      <h1><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h1>
    </article>
  <?php endif; ?>
</section>
<script type="text/javascript">
  function goToWorkDetails(itemIndex) {
    var offsetTopMargin = itemIndex === 0 && window.subMenuFixed? 80: 160;
    var moveTo = document.querySelectorAll('.work-details-item')[itemIndex].offsetTop - (offsetTopMargin - 1);

    jQuery('html,body').animate({ scrollTop:  moveTo}, 400);
  }
</script>