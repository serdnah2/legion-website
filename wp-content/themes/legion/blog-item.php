<section class="blog-item-section1" style="background-image: url('<?php echo get_field('blog_item_full_image', get_the_ID() )['url']; ?>')">
  <article class="montserratbold">
    <?php echo get_the_title(); ?><br/>
  </article>
</section>

<section class="blog-item-section2 avenirNextLTPro-Regular" >
  <article>
    <?php echo the_content(); ?>

    <div class="blog-items-nav-wrapper">
      <?php
        $prev_post = get_previous_post(true);
        if (!empty( $prev_post )): ?>
            <?php echo previous_post_link('%link', '<button type="button" class="prev montserratregular big-button button-orange">Previous</button>', true, 'blog_items'); ?>
        <?php endif ?>
      <?php
        $next_post = get_next_post(true);
        if (!empty( $next_post )): ?>
            <?php echo next_post_link('%link', '<button type="button" class="next montserratregular big-button button-orange">Next</button>', true, 'blog_items'); ?>
      <?php endif ?>
    </div>
  </article>
</section>
