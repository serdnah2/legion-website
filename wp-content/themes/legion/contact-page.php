
<section class="contact-section1" style="background-image: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) )?>')">
  <article class="montserratbold"> 
    <?php echo get_post_meta( get_the_ID(), 'contact-section1-title', true) ?> <br/>
  </article>
</section>
<section class="sub-menu-wrapper">
  <div class="montserratregular sub-menu">
    <?php html5blank_nav('sub menu'); ?>
  </div>
</section>

<section class="contact-section2">
  <article>
    <?php gmwd_map( 1, 1); ?>
  </article>
</section>

<section class="montserratlight contact-section3">
  <article>
    <div>
      <h1 class="abril-fatfaceregular contact-title">Send us a message</h1>
      <?php echo do_shortcode( '[contact-form-7 id="202" title="Contact form 1"]' ); ?>
    </div>

    <div>
      <h1 class="abril-fatfaceregular contact-title">Get in touch</h1>
      <div class="contact-page">
        <div class="contact-page-data">
          <img src="<?php echo get_field('contact_us_address_icon')['url']?>"/>
          <?php echo get_theme_mod('html5_address'); ?>
        </div>
        <div class="contact-page-data">
          <img src="<?php echo get_field('contact_us_phone_icon')['url']?>"/>
          <a href="tel:<?php echo get_theme_mod('html5_phone'); ?>"><?php echo get_theme_mod('html5_phone'); ?></a>
        </div>
        <div class="contact-page-data">
          <img src="<?php echo get_field('contact_us_email_icon')['url']?>"/>
          <?php echo get_theme_mod('html5_email'); ?>
        </div>
        <div class="contact-page-data contact-page-media">
						<?php $facebook_link = get_theme_mod('html5_facebook'); ?>
						<?php if (isset($facebook_link) && $facebook_link != ''):?>
							<a target="_blank" href="<?php echo $facebook_link;?>">
								<img src="<?php echo get_field('contact_us_facebook_icon')['url']?>"/>
							</a>
						<?php endif;?>
            
            <?php $twitter_link = get_theme_mod('html5_twitter'); ?>
						<?php if (isset($twitter_link) && $twitter_link != ''):?>
							<a target="_blank" href="<?php echo $twitter_link;?>">
								<img src="<?php echo get_field('contact_us_twitter_icon')['url']?>"/>
							</a>
						<?php endif;?>
        </div>
      </div>
    </div>
  </article>
</section>
