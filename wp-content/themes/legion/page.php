<?php get_header(); ?>

	<main role="main">
		<?php if(is_front_page()):?>
			<?php get_template_part('home-page'); ?>
		<?php endif;?>
		<?php if(is_page('work')):?>
			<?php get_template_part('work-page'); ?>
		<?php endif;?>
		<?php if(is_page('about')):?>
			<?php get_template_part('about-page'); ?>
		<?php endif;?>
		<?php if(is_page('services')):?>
			<?php get_template_part('services-page'); ?>
		<?php endif;?>
		<?php if(is_page('blog')):?>
			<?php get_template_part('blog-page'); ?>
		<?php endif;?>
		<?php if(is_page('contact-us')):?>
			<?php get_template_part('contact-page'); ?>
		<?php endif;?>
		<?php if(is_page('team')):?>
			<?php get_template_part('team-page'); ?>
		<?php endif;?>
	</main>

<?php get_footer(); ?>
