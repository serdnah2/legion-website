<section class="about-section1" style="background-image: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) )?>')">
  <article class="montserratbold">
    <?php echo get_post_meta( get_the_ID(), 'about_title', true) ?> <br/>
  </article>
</section>

<section class="sub-menu-wrapper">
  <div class="montserratregular sub-menu">
    <?php html5blank_nav('sub menu'); ?>
  </div>
</section>

<section class="about-section2">
  <article>
    <h1 class="montserratregular">
      <?php echo get_post_meta( get_the_ID(), 'about_section2_title', true) ?> <br/>
    </h1>
    <span class="montserratregular">
      <?php echo get_post_meta( get_the_ID(), 'about_section2_description1', true) ?> <br/>
    </span>
    <span class="montserratregular">
      <?php echo get_post_meta( get_the_ID(), 'about_section2_description2', true) ?> <br/>
    </span>
  </article>
</section>

<section class="about-section3" style="background-image: url('<?php echo get_field('offer_background')['url']?>')"> 
  <article>
    <span class="montserratregular">
      Mobile is now
    </span>
    <h1 class="abril-fatfaceregular">
      <?php echo get_post_meta( get_the_ID(), 'about_section3_title', true) ?> <br/>
    </h1>
    <span class="montserratregular">
      <?php echo get_post_meta( get_the_ID(), 'about_section3_description', true) ?> <br/>
    </span>
    <div>
      <div>
        <h2 class="montserratsemi_bold">
          <?php echo get_post_meta( get_the_ID(), 'about_section3_sub_title1', true) ?> <br/>
        </h2>
        <div class="montserratlight">
          <?php echo get_post_meta( get_the_ID(), 'about_section3_sub_description1', true) ?> <br/>
        </div>
      </div>
      <div>
        <h2 class="montserratsemi_bold">
          <?php echo get_post_meta( get_the_ID(), 'about_section3_sub_title2', true) ?> <br/>
        </h2>
        <div class="montserratlight">
          <?php echo get_post_meta( get_the_ID(), 'about_section3_sub_description2', true) ?> <br/>
        </div>
      </div>
      <div>
        <h2 class="montserratsemi_bold">
          <?php echo get_post_meta( get_the_ID(), 'about_section3_sub_title3', true) ?> <br/>
        </h2>
        <div class="montserratlight">
          <?php echo get_post_meta( get_the_ID(), 'about_section3_sub_description3', true) ?> <br/>
        </div>
      </div>
    </div>
  </article>
</section>

<section class="about-section4" style="background-image: url('<?php echo get_field('get_in_touch_background')['url']?>')">
  <article>
    <div>
      <span class="montserratregular">
        <?php echo get_post_meta( get_the_ID(), 'about_section4_description', true) ?>
      </span>
      <h1 class="montserratsemi_bold get-in-touch-btn">
          <button type="button" class="montserratregular big-button button-orange" onclick="location.href='<?php echo get_field('get_in_touch_link'); ?>';">
            <?php echo get_post_meta( get_the_ID(), 'about_section4_title', true) ?>
          </button>
      </h1>
    </div>
  </article>
</section> 

