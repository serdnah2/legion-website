<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'legion'); //legion_wordpress

/** MySQL database username */
define('DB_USER', 'root'); //legion_wordpress

/** MySQL database password */
define('DB_PASSWORD', ''); //1297899612Appl

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '(Xe3J#EY1|AUPT!%283@##QXb&DT@V|U@?Lc]A0ml(U(kgV6pq1)vk~yG+5TpS0R');
define('SECURE_AUTH_KEY',  '7m_$?p0RL+BAFVlt| +SC[q2Hsp:K5At&-2k)8bTc(!ho2w3w3q@d#bz@}JFG{+F');
define('LOGGED_IN_KEY',    'qC{$_rHG21Y$jk~d+#;IFk)Z-N%^LQjcrCTK-I@v:5LdNU=a$mP^EI 5y}@ki_e{');
define('NONCE_KEY',        '>a27%= [Tz{0qip]</`z]CS9G|aH?N7-u-,-fP y>YKX;JpfG>x,pK:d9:Y(B)c8');
define('AUTH_SALT',        'Bvy-AlKcjO{Ga 2-i%|rHWT|9^gm7wbJIvN+u)#I6<T.+@C?Qns(xKS?(L&-0w7j');
define('SECURE_AUTH_SALT', ')*y<.gED.%W1ty><CziIFc.fHX{}i5-umy@)e?BFQf4DG,nHz>yjI&yTxt+5k=] ');
define('LOGGED_IN_SALT',   'Zy$TABet?^ht*%_/xGo&Z?&]/YoUeQ,/6uQTv]e5p|vj5#@XK4PV7tD+Sz+Va29@');
define('NONCE_SALT',       '%uHYTV8`s-WkWr@#Ku+#G9GLD){;bZ| seHiw9rr,M]*) |Orn/a;Du,(/_NB6?>');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
