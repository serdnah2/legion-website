<section class="work-section work-section1" style="background-image: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) )?>')"> 
  <article class="montserratsemi_bold">
    <?php echo get_post_meta( get_the_ID(), 'work_section1_title', true) ?> <br/>
    <div class="montserratregular">
      <?php echo get_post_meta( get_the_ID(), 'work_section1_description', true) ?> <br/>
    </div>
  </article>
</section>

<?php 
  $category_id = get_cat_ID('work_item');
  $args = array(
      'offset' => 0,
      'category' => $category_id,
      'orderby' => 'post_date',
      'order' => 'ASC',
      'post_type' => 'post',
  );
  $cases = wp_get_recent_posts($args);
?>
<?php foreach ($cases as $case) : ?>
  <section class="work-section" style="background-image: url('<?php echo get_field('work_item_full_img', $case['ID'])['url'];?>')">
    <article class="montserratbold">
      <?php echo $case['post_title']; ?><br/>
      <div class="montserratregular">
        <?php echo get_field('work_item_desc', $case['ID']);?><br/>
      </div>
      <div>
        <button type="button" class="montserratregular big-button button-orange" onclick="location.href='<?php echo get_permalink($case['ID']);?>';">VIEW CASE STUDY</button>
      </div>
    </article>
  </section>
<?php endforeach; ?>
